@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2 class="">Cotizaciones Nacionales</h2>
            </div>
            <div class="card-body">
                <quots :user="{{$user}}"></quots>
            </div>
        </div>
    </div>
</div>
@endsection