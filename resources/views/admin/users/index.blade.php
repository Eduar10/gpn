@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="container">
        <div class="row py-4">
            <div class="col">

            </div>
            <div class="col-md-auto">

            </div>
            <div class="col col-lg-2">
                <a type="button" href="/admin/quotations" class="btn btn-success">Cotizar</a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Cantidad de Guias</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <th scope="row">
                                    <a href="/admin/users/{{ $user['id'] }}/show">{{ $user['name'] }}</a>
                                </th>
                                <td> {{ $user['phone'] }} </td>
                                <td> <p class="btn btn-warning shadow"> {{ $user['ships'] }} </p> </td>
                                {{-- <td>
                                    <a class="btn btn-danger" href="/ship/{{$ship->id}}/update">Delete</a>
                                </td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection