@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row py-4">
                            <div class="col">

                            </div>
                            <div class="col-md-auto">
                                <a type="button" href="/admin/users/{{$user->id}}/quotations/create" class="btn btn-warning">Cotizar</a>
                            </div>
                            <div class="col col-lg-2">
                                <a type="button" href="/admin/create" class="btn btn-success">Create</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table DataTable">
                                <thead>
                                    <tr>
                                        <th scope="col"> Cotización </th>
                                        <th> Nombre </th>
                                        <th> Remitente </th>
                                        <th> Destinatario </th>
                                        <th> Dimensiones </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($quotations as $quotation)
                                    <tr>
                                        <td> {{ $quotation->user_id }} </td>
                                        <td> {{ $quotation->name }} </td>
                                        <td>
                                            {{ $quotation->dimension ? $quotation->dimension->zip_from : '' }}
                                        </td>
                                        <td>
                                            {{ $quotation->dimension ? $quotation->dimension->zip_to : '' }}
                                        </td>
                                        <td>
                                            {{ $quotation->dimension ?
                                                $quotation->dimension->height .'cm x '. $quotation->dimension->width .'cm x '. $quotation->dimension->length .'cm' : ''
                                            }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection