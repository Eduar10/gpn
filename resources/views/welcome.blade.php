@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row pt-5">
        <div class="col col-md-8">
            <div class="">
                <h1 class="pt-5 text-h1-gpn">Cotiza tus envíos nacionales</h1>
                <h2 class="pt-3 ps-2">con las mejores paqueterías del mercado</h2>
                <a class="mt-4 btn rounded-pill ps-3 pt-2 px-3 btn-gpn" href="/crear">Cotizar Envíos</a>
            </div>
        </div>
        <div class="col col-md-4">
            <img src="/img/guias_prepagadas_nacionales_ilustration1.png" width="100%" alt="">
        </div>
    </div>
</div>
@endsection