<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dimension extends Model
{
    use HasFactory;

    protected $fillable = [
        'quotation_id',
        'zip_from',
        'zip_to',
        'weight',
        'height',
        'width',
        'length'
    ];

    public function quotation()
    {
        return $this->belongsTo(Quotation::class);
    }

}
