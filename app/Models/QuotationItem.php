<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'amount_local',
        'quotation_id',
        'currency_local',
        'provider',
        'service_level_name',
        'service_level_code',
        'days',
        'insurable',
        'out_of_area_service',
        'out_of_area_pricing',
        'total_pricing',
        'is_occure',
        'sale_price'
    ];

    protected $guarded = [];

    public function quotation()
    {
        return $this->belongsTo(Quotation::class);
    }

}
