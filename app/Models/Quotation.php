<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'active'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function dimension()
    {
        return $this->hasOne(Dimension::class);
    }

    public function items()
    {
        return $this->hasMany(QuotationItem::class);
    }

}
