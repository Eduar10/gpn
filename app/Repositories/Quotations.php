<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\Models\Dimension;
use App\Models\Quotation;
use App\Models\QuotationItem;
use App\Skydropx\SkydropxApi;
use Illuminate\Support\Facades\Auth;

class Quotations {

	private $response;

	public function getQuotations($request)
	{
		return $this->getQuotationsSkydropx($request);

	}

	private function getQuotationsSkydropx($request)
	{
        $this->response = (new SkydropxApi)->getQuotations($request);

        if (isset($this->response['errors'])) {
            return [
                'error' => '417',
                'message' => 'No se pudo cotizar, revisa la información que ingresaste.'
            ];
        }

        if ($request->user_id) {
            $quotation = $this->storeQuotation($request->user_id);
            $this->storeQuotationItems($quotation);
            $this->storeDimensions($quotation, $request);
        }

        return collect($this->response)->map(function ($quotation) {
            if ($quotation['provider'] != 'VENCEDOR' && $quotation['provider'] != 'CARSSA' && $quotation['provider'] != 'AMPM') {
                return [
                    'name' => $quotation['provider'],
                    'type' => $quotation['service_level_code'] == 'ECOEXPRESS' || $quotation['service_level_code'] == 'ESTAFETA_STANDARD' || $quotation['service_level_code'] == 'FEDEX_EXPRESS_SAVER'  || $quotation['service_level_code'] == 'STANDARD' ? 'Terrestre': 'Express',
                    'date' => $this->dates($quotation),
                    'out_of_area_service' => $quotation['out_of_area_service'],
                    'out_of_area_pricing' => $quotation['out_of_area_pricing'],
                    'price_skydropx' => round($quotation['total_pricing']),
                    'price' => round($quotation['total_pricing'] + ($quotation['total_pricing'] * 0.50)),
                    'provider_logo' => $this->getUrlProviderLog($quotation)
                ];
            };
        });
	}

    private function storeDimensions($quotation, $request)
    {
        Dimension::create([
            'quotation_id' => $quotation->id,
            'zip_from' => $request->zip_from,
            'zip_to' => $request->zip_to,
            'weight' => $request->weight,
            'height' => $request->height,
            'width' => $request->width,
            'length' => $request->length
        ]);
    }


    private function storeQuotation($user_id)
    {
        return Quotation::create(['user_id' => $user_id]);
    }


    private function storeQuotationItems($quotation)
    {
        $quotations = collect($this->response)->map(function ($quotation) {
            return [
                'amount_local' => $quotation['amount_local'],
                'currency_local' => $quotation['currency_local'],
                'days' => $quotation['days'],
                'insurable' => $quotation['insurable'],
                'is_ocurre' => $quotation['is_ocurre'] == false ? 0 : 1,
                'out_of_area_pricing' => $quotation['out_of_area_pricing'],
                'out_of_area_service' => $quotation['out_of_area_service'],
                'provider' => $quotation['provider'],
                'service_level_code' => $quotation['service_level_code'],
                'service_level_name' => $quotation['service_level_name'],
                'total_pricing' => $quotation['total_pricing'],
                'sale_price' => round($quotation['total_pricing'] + ($quotation['total_pricing'] * 0.50))
            ];
        })->toArray();

        $quotation->items()->createMany($quotations);
    }


    private function dates($quotation)
    {
        Carbon::setLocale('es');
        return Carbon::now()->addDay($quotation['days'])->format("d M,Y");
    }


	private function getUrlProviderLog($quotation)
    {
        if ($quotation['provider'] == 'FEDEX') {
            return '/img/logos/1.png';
        }
        if ($quotation['provider'] == 'ESTAFETA') {
            return '/img/logos/2.png';
        }
        if ($quotation['provider'] == 'DHL') {
            return '/img/logos/3.png';
        }
        if ($quotation['provider'] == 'NINETYNINEMINUTES') {
            return '/img/logos/4.png';
        }
        if ($quotation['provider'] == 'PAQUETEXPRESS') {
            return '/img/logos/5.png';
        }
        if ($quotation['provider'] == 'REDPACK') {
            return '/img/logos/6.png';
        }
        if ($quotation['provider'] == 'UPS') {
            return 'img/logos/7.png';
        }
    }

}


