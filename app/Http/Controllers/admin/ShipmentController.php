<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $response = (new SkydropxApi)->getShipments();

        // $shipments = $collection['data'];

        if ( Auth::user()->isAdmin())
        {
            $ships = Ship::all();
            return view('admin.ships.index', compact('ships'));
        }

        return redirect('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all();
        $providers = Provider::all();
        $ships = Ship::all();
        $types = Type::all();
        $parcels = Parcel::all();
        return view('admin.ships.create', compact('ships', 'users', 'providers', 'parcels', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Ship::create([
            'number_ship' => $request->number_ship,
            'user_id' => $request->user_id,
            'provider_id' => $request->provider_id,
            'parcel_id' => $request->parcel_id,
            'type_id' => $request->type_id,
            'cost' => $request->cost,
            'price' => $request->price,
            'status' => 1
        ]);

        $ships = Ship::where('status', 1)->get();
        return view('admin.ships.index', compact('ships'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
