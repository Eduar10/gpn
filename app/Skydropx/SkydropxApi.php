<?php

namespace App\Skydropx;


class SkydropxApi {

	public function getAuthToken()
	{
		return (new GetTokenApi)->handle();
	}

	public function getShipments()
	{
		return (new GetShipments)->handle();
	}

	public function getQuotations($request)
	{
		return (new GetQuotations($request))->handle();
	}

}