<?php

namespace App\Skydropx;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;


class GetShipments
{

	const REQUEST_URL = 'https://api.skydropx.com/v1/shipments';
	const token = '3vGkLFjpIT5lhJbk8o7BvjhSYf3QhCv2r8y5WwqHlPwt';
    private $data;
    private $response;

	public function __construct()
	{
		// $this->foo = $foo;
	}

	public function getToken()
	{
		// return (new EnviaApi)->getAuthToken();
	}


	public function handle()
	{
		return $this->submitRequest();
	}


	public function submitRequest()
	{
		$http = new Client;

        try {

            $response = $http->get(self::REQUEST_URL, [
                'headers' => [
                    'Authorization' => 'Bearer '.self::token
                ],
                'timeout' => 180
            ]);

        } catch (\Exception $e) {

            info('Get Shipments: => ' . $e->getMessage());

        }

        return $this->response = json_decode((string) $response->getBody(), true);

	}

}