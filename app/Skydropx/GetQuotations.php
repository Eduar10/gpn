<?php

namespace App\Skydropx;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth;


class GetQuotations
{

	const REQUEST_URL = 'https://api.skydropx.com/v1/quotations';
	const token = '3vGkLFjpIT5lhJbk8o7BvjhSYf3QhCv2r8y5WwqHlPwt';
	private $request;
    private $data;
    private $response;

	public function __construct($request)
	{
		$this->request = $request;
		$this->buildData();
	}

	public function getToken()
	{
		// return (new EnviaApi)->getAuthToken();
	}

	public function buildData()
	{
		$this->data = [
            'zip_from' => $this->request->zip_from,
            'zip_to' => $this->request->zip_to,
            'parcel' => [
                'weight' => $this->request->weight,
                'height' => $this->request->height,
                'width' => $this->request->width,
                'length' => $this->request->length
            ]
        ];
	}

	public function handle()
	{
		return $this->submitRequest();
	}


	public function submitRequest()
	{
		$http = new Client;

        try {

        	$response = Http::timeout(180)->withToken(self::token)
                ->post(self::REQUEST_URL, $this->data);

        } catch (\Exception $e) {

            info('Get Shipments: => ' . $e->getMessage());

        }

        return $this->response = json_decode((string) $response->getBody(), true);

	}

}