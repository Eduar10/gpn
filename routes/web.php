<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\admin\QuotationController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return Auth::user()->isAdmin()
        ? redirect('admin/users')
        : redirect('crear');
})->name('home');

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('crear', 'QuotationController@create');
});


Route::prefix('admin')->middleware('auth')->group(function () {
    Route::get('users',[UserController::class, 'index']);
    Route::get('users/{id}/show', [UserController::class, 'show']);
    Route::get('quotations', [QuotationController::class, 'index']);
    Route::get('users/{id}/quotations/create', [QuotationController::class, 'create']);
    Route::post('quotations/store', [QuotationController::class, 'store']);
    // Route::get('ships', 'admin\ShipmentController@index');
    // Route::get('create', 'admin\ShipmentController@create');
    // Route::post('store', 'admin\ShipmentController@store');
    // Route::get('destroy', 'admin\ShipmentController@destroy');
    // Route::get('{id}/update', 'admin\ShipmentController@update');
});
