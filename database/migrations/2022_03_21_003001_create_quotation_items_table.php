<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('quotation_id');
            $table->string('amount_local');
            $table->string('currency_local');
            $table->string('provider');
            $table->string('service_level_name');
            $table->string('service_level_code');
            $table->integer('days');
            $table->boolean('insurable');
            $table->boolean('out_of_area_service');
            $table->string('out_of_area_pricing');
            $table->string('total_pricing');
            $table->boolean('is_occure')->default(false);
            $table->string('sale_price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_items');
    }
};
